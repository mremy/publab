The purpose of this tiny code is to solve the issue of using the *mkstemp()*
function without deleting the temporary file, by adding an intermediate function
with the LD_PRELOAD trick (which, obviously, should be applicable), to call the
*unlink()* function.

As stated on Stack Overflow (accepted answer to question 32445579), it works
**only** if the temporary file is acceded only through the file descriptor in
the program and not on the file system level by is name.
