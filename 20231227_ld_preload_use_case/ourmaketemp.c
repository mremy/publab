/* # SOF */

#include <dlfcn.h>
#include <unistd.h>

int mkstemp( char *template ) {
	int fd;
  fd = ( ( int (*)(char *) )dlsym( RTLD_NEXT, "mkstemp" ) )( template );
	if( fd != 0 ) {
		unlink( template );
	}
	return fd;
}

int mkostemp( char *template, int flags ) {
	int fd;
  fd = ( ( int (*)(char *, int) )dlsym( RTLD_NEXT, "mkostemp" ) )( template, flags );
	if( fd != 0 ) {
		unlink( template );
	}
	return fd;
}

int mkstemps( char *template, int suffixlen ) {
	int fd;
  fd = ( ( int (*)(char *, int) )dlsym( RTLD_NEXT, "mkstemps" ) )( template, suffixlen );
	if( fd != 0 ) {
		unlink( template );
	}
	return fd;
}

int mkostemps( char *template, int suffixlen, int flags ) {
	int fd;
  fd = ( ( int (*)(char *, int, int) )dlsym( RTLD_NEXT, "mkostemps" ) )( template, suffixlen, flags );
	if( fd != 0 ) {
		unlink( template );
	}
	return fd;
}

/* # EOF */
