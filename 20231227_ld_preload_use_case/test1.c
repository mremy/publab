/* # SOF */
#define _POSIX_C_SOURCE 200809L
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define CNIJPWG_TEMP "/var/tmp/cnijpwgtmpXXXXXX"

int main(void) {
	int fd;
	char tmpStr[64];
	strncpy(tmpStr, CNIJPWG_TEMP, 63 );

	fd = mkstemp( tmpStr );
	if (!fd) {
		printf(" mkstemp() returned NULL\n" );
		return 1;
	}

	exit( EXIT_SUCCESS );
}
/* # EOF */
