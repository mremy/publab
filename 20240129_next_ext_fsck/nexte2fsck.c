/* # SOF
 *
 * %Begin-Header%
 * This file may be redistributed under the terms of the GNU Public
 * License.
 * %End-Header%
 */

#include <time.h>
#include <ext2fs/ext2fs.h>
#include <e2p/e2p.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>

static const char * program_name = "nexte2fsck";
static char * device_name = NULL;

#include "interval_string.c"

int main (int argc, char ** argv)
{

	/* Only one argument for first draft, the FS name */
	if( argc!=2 )
	{
		fprintf (stderr, "Missing device name.\n");
		return (EXIT_FAILURE);
	}

  errcode_t retval;
	errcode_t retval_csum = 0;
  ext2_filsys fs;
  blk64_t   use_superblock = 0;
  int   use_blocksize = 0;
  int flags;
	time_t  tm;

  if (argc && *argv) {
    if (strrchr(*argv, '/'))
      program_name = strrchr(*argv, '/') + 1;
    else
      program_name = *argv;
	}

  device_name = argv[1];
  flags = EXT2_FLAG_JOURNAL_DEV_OK | EXT2_FLAG_SOFTSUPP_FEATURES |
    EXT2_FLAG_64BITS | EXT2_FLAG_THREADS | EXT2_FLAG_SUPER_ONLY;

try_open_again:
  retval = ext2fs_open( device_name, flags, use_superblock,
           use_blocksize, unix_io_manager, &fs);

  flags |= EXT2_FLAG_IGNORE_CSUM_ERRORS;

	if( retval  && !retval_csum ) {
		retval_csum = retval;
		goto try_open_again;
	}
	if (retval) {
		fprintf( stderr, "Error opening RO %s\n", device_name );
		return (EXIT_FAILURE);
	}
	fprintf( stdout, "Mount count:              %u\n", fs->super->s_mnt_count);
  fprintf( stdout, "Maximum mount count:      %d\n", fs->super->s_max_mnt_count);
  tm = fs->super->s_lastcheck;
  fprintf( stdout, "Last checked:             %s", ctime(&tm));
  fprintf( stdout, "Check interval:           %u (%s)\n", fs->super->s_checkinterval,
         interval_string(fs->super->s_checkinterval));

	ext2fs_close( fs );
	return (EXIT_SUCCESS);
}

/* # EOF */
